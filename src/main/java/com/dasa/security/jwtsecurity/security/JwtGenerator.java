package com.dasa.security.jwtsecurity.security;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dasa.security.jwtsecurity.model.JwtUserLogin;
import com.dasa.security.jwtsecurity.model.JwtUserResponse;
import com.dasa.security.jwtsecurity.user.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Component
public class JwtGenerator {

	@Autowired
	JwtValidator jwtValidator;

	@Autowired
	User user;

	public JwtUserResponse generate(JwtUserLogin jwtUserLogin) {

		if (user.validate(jwtUserLogin.getUsername(), jwtUserLogin.getPassword())) {

			JwtUserResponse jwtUserResponse = new JwtUserResponse();

			Claims claims = Jwts.claims().setSubject(user.retriveInfoByUser(jwtUserLogin.getUsername()).getUserName());

			// Optionals
			claims.put("id", String.valueOf(user.retriveInfoByUser(jwtUserLogin.getUsername()).getId()));
			claims.put("role", user.retriveInfoByUser(jwtUserLogin.getUsername()).getRole());
			//

			String jwt = Jwts.builder().setClaims(claims).signWith(JwtValidator.key)
					// Optionals
					.setExpiration(expirationDateInMinutes(10)).setHeaderParam("typ", "JWT")
					.setIssuer("http://medicos.dasa.com.br").compact();
			//

			jwtUserResponse.setJwt(jwt);

			return jwtUserResponse;

		} else {
			throw new IllegalArgumentException("Usuário ou senha inválidos");
		}
	}

	private Date expirationDateInMinutes(Integer qtd) {
		Calendar now = Calendar.getInstance();
		now.add(Calendar.MINUTE, qtd);
		return now.getTime();
	}
}
