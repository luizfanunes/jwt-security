package com.dasa.security.jwtsecurity.security;

import java.security.Key;

import org.springframework.stereotype.Component;

import com.dasa.security.jwtsecurity.model.JwtUser;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;

@Component
public class JwtValidator{
	
	public static Key key = Keys.secretKeyFor(SignatureAlgorithm.HS512);


    public JwtUser validate(String token) throws ExpiredJwtException, SignatureException {

        JwtUser jwtUser = null;

            Claims body = Jwts.parser()
                    .setSigningKey(key)
                    .parseClaimsJws(token)
                    .getBody();

            jwtUser = new JwtUser();

            jwtUser.setUserName(body.getSubject());
            jwtUser.setId(Long.parseLong((String) body.get("id")));
            jwtUser.setRole((String) body.get("role"));        
      

        return jwtUser;
    }
}
