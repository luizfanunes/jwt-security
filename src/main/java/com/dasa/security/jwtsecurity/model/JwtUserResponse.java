package com.dasa.security.jwtsecurity.model;

import lombok.Data;

@Data
public class JwtUserResponse {

	private String jwt;
}
