package com.dasa.security.jwtsecurity.model;

import lombok.Data;

@Data
public class JwtUser {

	private String userName;

	private long id;

	private String role;
}
