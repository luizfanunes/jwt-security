package com.dasa.security.jwtsecurity.model;

import lombok.Data;

@Data
public class JwtUserLogin {
	
	private String username;
	private String password;
}
