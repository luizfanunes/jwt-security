package com.dasa.security.jwtsecurity.user;

import org.springframework.stereotype.Component;

import com.dasa.security.jwtsecurity.model.JwtUser;

@Component
public class User {

	public boolean validate(String user, String password) {
		if (user.equals("user") || user.equals("user1"))
			return true;
		return false;
	}

	public JwtUser retriveInfoByUser(String username) {
		JwtUser jwtUser = new JwtUser();
		if (username.equals("user")) {
			jwtUser.setId(1L);
			jwtUser.setRole("ROLE_ADMIN");
			jwtUser.setUserName("Luiz Felipe");
		} else {
			jwtUser.setId(2L);
			jwtUser.setRole("ROLE_USER");
			jwtUser.setUserName("Adauto Nobrega");
		}

		return jwtUser;
	}

}
