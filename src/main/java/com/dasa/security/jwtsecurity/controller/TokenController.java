package com.dasa.security.jwtsecurity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dasa.security.jwtsecurity.model.JwtUserLogin;
import com.dasa.security.jwtsecurity.security.JwtGenerator;

@RestController
@RequestMapping("/token")
public class TokenController {

	@Autowired
	private JwtGenerator jwtGenerator;

	@PostMapping
	public ResponseEntity<Object> generate(@RequestBody final JwtUserLogin jwtUserLogin) {

		try {
			return new ResponseEntity<>(jwtGenerator.generate(jwtUserLogin), HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (Exception e) {
			return new ResponseEntity<>("Falha ao gerar Token", HttpStatus.UNAUTHORIZED);
		}

	}
}
